 #!/usr/bin/env python

# ################################################################################################
# 2nd hand owner: MoDyCo
# Adaptation: Nicolas Foucault -- IR MoDyCo (Lyrics project + ComNum Labex)
# Adaptation2: Brigite Juanals -Jean-Luc Minel--  MoDyCo 
# collecte l'url de la photo
# Licence: GNU GPL version 3 (temporary; ask  Brigite Juanals or Jean-Luc Minel for final decision about Licence type)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Original owner: edsu
# Original name: json2csv.py
# Original licence: CC0 1.0 Universal (Creative Commons Legal Code)
# Reference: https://github.com/edsu/twarc
# ################################################################################################
# Description | This script is used to convert tweet data from json to csv format. It requires two
# parameters: the JSON file to be converted (absolute reference) and the output file name where to
# store the CSV convertion (absolute reference as well). Input and output data are both encoded in
# UTF-8 (w/o unicode characters). Multivalued JSON properties becomes space delimited CSV columns.
# ------------------------------------------------------------------------------------------------
# Data type   | Mostly textual w/ multi-types.
# ------------------------------------------------------------------------------------------------
# Data format | Input: JSON (UTF-8 w/o unicode char); Output: CSV (UTF-8 w/o unicode char).
# ################################################################################################

# usage: e.g. ./json2csv.py <I/path2json> <O/path2csv> > /dev/null

import sys             # NEW (compared to the original github code)
import json
#import codecs	       # NEW
import fileinput
import mongo
from pymongo import MongoClient
import os
from pytz import timezone
from datetime import datetime
import time

if sys.version_info[0] < 3:
    import unicodecsv as csv
else:
    import csv

inputfile=sys.argv[1]  # NEW
dbMongo = sys.argv[2]  # La base Mongo

def inperiodOfTime(t) :
	paristime = timezone('Europe/Paris')
	utc = timezone('UTC')
	tweet_created_at = datetime.strptime(t[1], '%a %b %d %H:%M:%S +0000 %Y')
	paris_created_at = paristime.localize(tweet_created_at)
#pour la periode avant  2017-05-20 18:00:00+00:00
# modifier les tests pour creer les bases Before, Event and After
	paris_seuil = datetime(2016, 5, 21, 18, 0, 0, tzinfo=paristime)
	l=str(paris_created_at).split('-')
	day=str(l[2]).split(' ')
	intday=int(str(day[0]))
	if intday <20 :
		return True
	if intday== 20 :
			lh0=str(l[2]).split(' ')
			lheure=str(lh0[1]).split(':')
			intheure=int(str(lheure[0]))
			if  intheure < 18 :
				return True
			else :
				return False
	if intday > 20 :
		return False
	return False

def notexist(t, collection):
    id_t=t['id_str']
    cursor=collection.find({'id_str' : id_t}).count()
    if cursor == 0 :
        return True
    else :
        return False

def connectmongo(dbMongo) :
        client = MongoClient('localhost', 27017)
        try :
            db = client[dbMongo]
            print("Connexion base: ", dbMongo)
        except :
            print("no connection")
            exit()
        return db
    	
# -----------------------------  MAIN ----------------------------------------	

def main():
    start=time.time()
    i=0 
    db = connectmongo(dbMongo)
    collectionTweet = db[dbMongo]
    print("ATTENTION, ajout dans la base existante sans confirmation : ", dbMongo)
    print("A partir du fichier : ", inputfile)
    doublon = 0	
    try :		
        for  line in fileinput.input():      # fix inline bkline pbi=i+1
            i=i+1
            s = line.replace(r'\n',r'\\n')         # NEW
            try :
                tweet = json.loads(s)
                	# if inperiodOfTime(data) :
                notexistTweet = notexist(tweet, collectionTweet)
                if notexistTweet:
                    try :
                        insert_tweet=collectionTweet.insert_one(tweet)                  # NEW
                    except Exception as e :
                        print ("not inserting", str(e))
                else :
                    doublon=doublon+1
            except Exception as e :
                print ("erreur lecture ligne", i, str(e))
                pass
    except :
        print ("end of file", i)
        pass

    count_twit= collectionTweet.count() 
    print("doublon : ", doublon)   
    print ("nb tweet dans la base", count_twit)
    print ("dans la collection Mongo : ", dbMongo)

    end = time.time() - start
    print ("Time to complete:", end)
    print ('Done')

	


#       +++++++++++++++++++++++++++++++  STARTING POINT +++++++++++++++++++++++++++++
if __name__ == "__main__":
    main()
    print (' traitement fichier : termine')
