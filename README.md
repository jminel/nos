# NOS

La plateforme NOS (Nouvel Outil statistique) est une plateforme de collecte, de traitement et d’analyse de corpus de textes issus des réseaux sociaux numériques. Elle a été développée pour les partenaires du Labex PASP- Le Passé dans le Présent (ANR-11-LABX-0026-01), Université de Nanterre dans le cadre du Projet ANR-Labex PASP-Comm-Num (2014-2019), **coordonné par Brigite Juanals**,  « Médiation culturelle et circulation des connaissances à l’ère numérique : communication institutionnelle, pratiques éditoriales, pratiques informationnelles et dispositifs socio-techniques », en partenariat avec des institutions patrimoniales (notamment le Musée du Quai Branly, le musée du Louvre, le Centre Pompidou, le musée de Cluny) et le Ministère de la culture et de la communication. Les composants logiciels sont déposés dans le « Gitlab Huma-Num » de la TGIR (Très Grande Infrastructure de Recherche) Huma-Num pour permettre leur accès et leur réutilisation.

# PUBLICATIONS
La conception et le « workflow » de la plateforme finalisée sont décrits dans l’article :

 Juanals, B., Minel J.-L., 2017 : 376-385, Analysing Cultural Events on Twitter. Thanh Nguyen N.; Papadopoulos G.A., Jedrzejowicz P., Trawinski B., Vossen G. Computational Collective Intelligence, Computational Collective Intelligence - Part II, Lecture Notes in Artificial Intelligence (10 449), Springer, 978-3-319-67076-8.

 Différentes utilisations de la plateforme NOS  sont décrites dans :

 - Brigitte Juanals, Jean-Luc Minel. Figures de l’autorité et formats médiatiques de l’information culturelle. L’exposition temporaire « Delacroix (1798-1863) », musée du Louvre. Études de communication - Langages, information, médiations, Université de Lille, 2020, La constitution des autorités numériques dans la production et la circulation de l’information, 55, pp.111-133. ⟨halshs-03140667⟩
 - Antoine Courtin, Brigitte Juanals, Jean-Luc Minel, Mathilde de Saint Léger. The “MuseumWeek” Event : Analyzing Social Network Interactions in Cultural Fields. The 10th International Conference on Signal Image Technology & Internet based Systems Workshop on Visions on Internet of Cultural Things and Applications, Nov 2014, Marrakech, Morocco. IEEE Computer Society, pp.462-468, 2014, Proceedings of the 10th International Conference on Signal Image Technology & Internet Systems (SITIS). Workshop Victa. ⟨halshs-01075444⟩
 - Brigitte Juanals, Jean-Luc Minel. Information Flow on Digital Social Networks during a Cultural Event: Methodology and Analysis of the "European Night of Museums 2016" on Twitter. Anatoliy Gruzd. 8th International Conference on Social Media & Society, Jul 2017, Toronto, Canada. ICPS- ACM, pp.1-12, 2017, Proceedings of the 8th International Conference on Social Media & Society, ⟨10.1145/3097286.3097299⟩. ⟨halshs-01568177⟩
 - Brigitte Juanals, Jean-Luc Minel. Categorizing Air Quality Information Flow on Twitter Using Deep Learning Tools. Lecture Notes in Artificial Intelligence, Springer, 2018, Computational Collective Intelligence, 11055, pp.109-118. ⟨halshs-01800457
 - Brigitte Juanals, Jean-Luc Minel. An Instrumented Methodology to Analyze and Categorize Information Flows on Twitter Using NLP and Deep Learning : A Use Case on Air Quality. Lecture Notes in Artificial Intelligence, Springer, 2018, Foundations of Intelligent Systems, pp.315-322. ⟨10.1007/978-3-030-01851-1⟩. ⟨halshs-01904917⟩ 
 - Antoine Courtin, Nicolas Foucault. Analyses quantitatives et catégorielles des tweets émis dans le cadre de l’évènement #MuseumWeek2015 : Projet ComNum , "Médiation culturelle et circulation des connaissances à l’ère numérique", coordonné scientifiquement par Brigitte Juanals (IRSIC). [Rapport de recherche] V0.2, Labex les passés dans le présent. 2015. ⟨halshs-01217118⟩ 


