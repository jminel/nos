#!/usr/bin/env python

# ################################################################################################
# 2nd hand owner: MoDyCo
# Adaptation: Brigitte Juanals, Jean-Luc Minel MoDyCo (Lyrics project + ComNum Labex)
# Licence: GNU GPL version 3 (temporary; ask Brigitte Juanals, or Jean-Luc Minel for final decision about Licence type)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Original owner: edsu
# Original name: 
# Original licence: CC0 1.0 Universal (Creative Commons Legal Code)
# Reference: 
# ################################################################################################
# Description | Analyse des dates
# dictionnaire :
# clef la dat du jour
# valeur : nombre de tweets

# ------------------------------------------------------------------------------------------------
# Data type   | Mostly textual w/ multi-types.
# ------------------------------------------------------------------------------------------------
# Data format | Input: JSON (UTF-8 w/o unicode char); Output: CSV (UTF-8 w/o unicode char).
# ################################################################################################

# 

import sys             # NEW (compared to the original github code)
import json
import codecs	       # NEW
import fileinput
import mongo
from pymongo import MongoClient
from pytz import timezone
import time
import datetime
import os
if sys.version_info[0] < 3:
    import unicodecsv as csv
else:
    import csv
class AnalyseDate :

	def __init__(self,nomdb, nomcollection, lan):
		client = MongoClient('localhost', 27017)
		self._db=client[nomdb]
		self._collection=self._db[nomcollection]
		self.utc = timezone('UTC')
		self._langue=lan
		
#conversion de date en UTC		
	def get_date(self,t) :
		created_at = datetime.datetime.strptime(t['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
		utc_created_at = self.utc.localize(created_at)
		return utc_created_at

#extraire le jour mois annee
	def get_jma(self,d):
		l=str(d).split()
		return str(l[0])

	def get_row(self,t):
		row=[get_date(t),t['user']['name'],t['full_text'] ]
		return row

# traitement des flux de tweets par jour
		
	def fluxjour(self, datadir, outputfile):
		if self._langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': self._langue})


		print ("nombre  tweet dans la base en langue :", self._langue, '   ',  cursor.count())	

	#on compte les tweets par jour
		dicoid={}
		for tweet in cursor:
			datetweet=self.get_jma(self.get_date(tweet))
			try:
				dicoid[datetweet]=dicoid[datetweet]+1
			except KeyError:
				dicoid[datetweet]=1

		ldiconame=dicoid.items()
		diroutputfile = datadir+outputfile
		with open(diroutputfile ,'w') as f:
			out = csv.writer(f, delimiter = ',')
			listedates=[] 
			for k in ldiconame:
				row=[k[0], k[1]]
				out.writerow(row)
				listedates.append(k[0])
			print ('flux de tweets par jour dans le fichier : ', diroutputfile )
        # on trie 
		f.close()
		dates = [datetime.datetime.strptime(ts,"%Y-%m-%d") for ts in listedates]
		dates.sort()
		sorteddates =  [datetime.datetime.strftime(ts,"%Y-%m-%d") for ts in dates]
        # on explore la liste pour retrouver les nombres d occurrences dans le dico
		i = 0
		outputfile2= datadir+'outputdatesorted.csv'
		with open(outputfile2,'w') as f2:
			out = csv.writer(f2,delimiter = ',')
			while i < len(sorteddates) :
				dday = sorteddates[i]
				nb = dicoid[dday]
				row = [dday, nb]
				out.writerow(row)
				i = i + 1
		print ('flux de tweets sorted  par jour dans le fichier : ', outputfile2)
		f2.close()

# traitement des flux de tweets originaux par jour
	def fluxjourtweet(self, datadir, outputfile):	    
	#   on traite que les tweets origianux francophones 
		if self._langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})

		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': self._langue})
	#on compte les tweets par jour
		dicoid={}
		for tweet in cursor:
			datetweet=self.get_jma(self.get_date(tweet))
			try:
				dicoid[datetweet]=dicoid[datetweet]+1
			except KeyError:
				dicoid[datetweet]=1

		ldiconame=dicoid.items()
		diroutputfile = datadir+outputfile
		with open(diroutputfile ,'w') as f:
			out = csv.writer(f, delimiter = '\t') 
			for k in ldiconame:
				row=[k[0], k[1]]
				out.writerow(row)
			print ('flux de tweets originaux par jour dans le fichier : ', diroutputfile )
# -------------------------------------------------------------------------------------------------
# traitement des flux de tweets originaux par jour
	def fluxSemaineTwo(self, datadir, outputfile):	    
	#   on traite que les tweets origianux francophones 
		if self._langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})

		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': self._langue})
	#on compte les tweets par jour
		dicoid={}
		for tweet in cursor:
			datetweet=self.get_jma(self.get_date(tweet))
			try:
				dicoid[datetweet]=dicoid[datetweet]+1
			except KeyError:
				dicoid[datetweet]=1

		dicoSemaine = {}
		for clef, value in dicoid.items() :
			# compute week number
			structureTime = time.strptime(clef,"%Y-%m-%d")
			numberSemaine = int(time.strftime("%W", structureTime )) + 1 #decalage semaine module time   
			if (numberSemaine in dicoSemaine) :				
				dicoSemaine[numberSemaine] =dicoSemaine[numberSemaine] + value
			else :
				dicoSemaine[numberSemaine] = 1
		diroutputfile = datadir + outputfile 
		with open(diroutputfile ,'w') as f:
			out = csv.writer(f, delimiter = ',')
			row = ["Semaine", "Nombre tweets originaux"]
			out.writerow(row) 
			for clef, value in dicoSemaine.items():
				row=[clef, value]
				out.writerow(row)
			print ('flux de tweets originaux par semaine dans le fichier : ', diroutputfile )


# traitement des flux de retweets par jour
	def fluxjouretweet(self, datadir, outputfile):	    
	#   on traite que les retweets  dans la langue 
		if self._langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': self._langue, 'retweeted_status': {"$ne" :None}})
	
		dicoid={}
		for tweet in cursor:
			datetweet=self.get_jma(self.get_date(tweet))
			try:
				dicoid[datetweet]=dicoid[datetweet]+1
			except KeyError:
				dicoid[datetweet]=1

		ldiconame=dicoid.items()
		diroutputfile =datadir + outputfile
		with open(diroutputfile ,'w') as f:
			out = csv.writer(f,delimiter = '\t') 
			for k in ldiconame:
				row=[k[0], k[1]]
				out.writerow(row)
			print ('flux de retweets par jour dans le fichier : ', diroutputfile )			

#on cherche les tweets d une date (jour mois anne)

	def get_tweet_date(self, datepic, outputfile) :
		if self._langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': self._langue, 'retweeted_status': {"$ne" :None}})
	

		with open(outputfile,'w') as f:
			out = csv.writer(f, delimiter = '\t') 
			for tweet in cursor:
				datetweet=get_jma(get_date(tweet)).strip()
				if (datetweet == datepic):
					out.writerow(self.get_row(tweet))
			print ('tweet de la date: ', datepic, ' dans le fichier : ', outputfile)
