#!/usr/bin/env python

# ################################################################################################
# 2nd hand owner: MoDyCo
# Adaptation: Jean-Luc Minel MoDyCo (Lyrics project + ComNum Labex)
# Licence: GNU GPL version 3 (temporary; ask Jean-Luc Minel for final decision about Licence type)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Original owner: edsu
# Original name: 
# Original licence: CC0 1.0 Universal (Creative Commons Legal Code)
# Reference: 
# ################################################################################################
# Description | Comptage des langues des tweets
# renvoie un dictionnaire
# clef : la langue
# valeur : nombre de tweets dans cette langue
# ------------------------------------------------------------------------------------------------
# Data type   | Mostly textual w/ multi-types.
# ------------------------------------------------------------------------------------------------
# Data format | Input: JSON (UTF-8 w/o unicode char); Output: CSV (UTF-8 w/o unicode char).
# ################################################################################################

# u

import sys             # NEW (compared to the original github code)
import codecs	       # NEW
import mongo
from pymongo import MongoClient
import os

class AnalyseLangue :
	def __init__(self,nomdb, nomcollection):
		client = MongoClient('localhost', 27017)
		self._db=client[nomdb]
		self._collection=self._db[nomcollection]

# on trouve la langue du tweet		
	def get_lang(self,t) :
		lang=t['metadata']['iso_language_code']
		return lang
	
	def countlang(self):   
#   on traite tous  les tweets  pour anlyser la langue
		cursor = self._collection.find()
		dicoid={}
		for tweet in cursor:
			lang=self.get_lang(tweet)
			try:
				dicoid[lang]=dicoid[lang]+1
			except KeyError:
				dicoid[lang]=1
				
		return dicoid

		

