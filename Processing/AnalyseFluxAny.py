# coding: utf8
#!/usr/bin/env python

# ################################################################################################
# 2nd hand owner: MoDyCo
# Adaptation: Brigitte Juanals, Jean-Luc Minel MoDyCo (Lyrics project + ComNum Labex)
# Licence: GNU GPL version 3 (temporary; ask Brigitte Juanals or Jean-Luc Minel for final decision about Licence type)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Original owner: edsu
# Original name: 
# Original licence: CC0 1.0 Universal (Creative Commons Legal Code)
# Reference: 
# ################################################################################################
# Description | Table croise :
# dictionnaire 
# clef : le id_string du tweet original
# valeur : deux listes ltweetoriginal,  ltweetretweet
# ltweetoriginal : id_str, name, id_str_useroriginal
# ltweetretweet : name, id_str_user_retweet
# ------------------------------------------------------------------------------------------------
# Data type   | Mostly textual w/ multi-types.
# ------------------------------------------------------------------------------------------------
# Data format | Input: JSON (UTF-8 w/o unicode char); Output: CSV (UTF-8 w/o unicode char).
# ################################################################################################
import sys             # NEW (compared to the original github code)
import json
import codecs	       # NEW
import fileinput
import mongo
from pymongo import MongoClient
from pytz import timezone
from datetime import datetime
import re, nltk
import os
if sys.version_info[0] < 3:
    import unicodecsv as csv
else:
    import csv

from  DictionnaireOrdonne import DictionnaireOrdonne
from AnalyseDate import AnalyseDate


	
class AnalyseTweet :
	""" Analyse des tweets"""

	
	def __init__(self,nomdb, nomcollection, fileOrga):
		client = MongoClient('localhost', 27017)
		self._db=client[nomdb]
		self._collection=self._db[nomcollection]
		self.utc = timezone('UTC')
		self._fileOrga = fileOrga


	def filtre_lang(self, la):
		cursor = self._collection.find({'lang': la})
		return cursor

	def get_row(self,t):
		row=[t['retweeted_status']['user']['name'],t['retweeted_status']['user']['id_str'], t['retweeted_status']['retweet_count'],t['retweeted_status']['full_text'] ]
		return row

	def get_row_withIdtweet(self,t):
		row=[t['id_str'],t['created_at'],t['retweeted_status']['user']['name'],t['retweeted_status']['user']['id_str'], t['retweeted_status']['retweet_count'],t['retweeted_status']['full_text'] ]
		return row

	def get_row_tweet(self,t):
		row=[t['created_at'],t['user']['name'],t['full_text'] ]
		return row

	def get_row_tweet_onlytext(self,t):
		row=[t['full_text'] ]
		print ("TWEET: ", row)
		return row


	def get_date(self,t) :
		created_at = datetime.strptime(t['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
		utc_created_at = self.utc.localize(created_at)
		return utc_created_at

	def get_date_tweet_original(self,tweet):
		cursor = self._collection.find({'id': tweet['retweeted_status']['id']}).count()
		if cursor == 0 : 
		# if we do not find original tweet, tdate is arbitrary set at Sun Jan 01 00:00:00 +0000 2016
			ch_created_at="Sun Jan 01 00:00:00 +0000 2016"
			created_at=datetime.strptime(ch_created_at, '%a %b %d %H:%M:%S +0000 %Y')
			utc_created_at = self.utc.localize(created_at)

		else:
			cursor = self._collection.find({'id': tweet['retweeted_status']['id']})		
			for t in cursor :
				created_at = datetime.strptime(t['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
				utc_created_at = self.utc.localize(created_at)
		return utc_created_at

# finding id_str tweet original from a tweet which is a retweet
	def getId_strTweetOriginalFromRetweet(self, tweet):
		cursor = self._collection.find({'id' : tweet['retweeted_status']['id']}).count()
		if cursor == 0 :
			return None
		else :
			cursor = self._collection.find({'id': tweet['retweeted_status']['id']})	
		for t in cursor :
			return t['id_str']


#date of original twet more simple
	def get_date_tweet_original2(self,tweet, modaledge):
		ch_created_at=tweet[modaledge]['created_at']
		created_at=datetime.strptime(ch_created_at, '%a %b %d %H:%M:%S +0000 %Y')
		return self.utc.localize(created_at)
		

		
# extracting day month year from date
	def get_jma(self,d):
		l=str(d).split()
		return str(l[0])
		
# extracting hour minute second  from date		
	def get_hms(self,d):
		l=str(d).split()
		l1=str(l[1]).split('+')
		return l1

# les comptes qui ont emis un tweet ou un retwwet en langue 
	def get_compte(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicouser={}
		for tweet in cursor:
			user=tweet['user']['name']
			try:
				dicouser[user]=dicouser[user]+1
				
			except KeyError:
				dicouser[user]=1
		return dicouser

# les id_str  des comptes qui ont emis un tweet ou un retwwet en langue 
	def get_compte_id_str(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicouser={}
		for tweet in cursor:
			user=tweet['user']['id_str']
			try:
				dicouser[user]=tweet['user']['id_str']
				
			except KeyError:
				dicouser[user]=tweet['user']['id_str']
		return dicouser

# les id_str et les names des comptes qui ont emis un tweet ou un retwwet en langue 
	def get_compte_id_str_name(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicouser={}
		for tweet in cursor:
			user=tweet['user']['id_str']
			try:
				dicouser[user]=tweet['user']['name']
				
			except KeyError:
				dicouser[user]=tweet['user']['name']
		return dicouser

# les id_str et les screen names des comptes qui ont emis un tweet ou un retwwet en langue 
	def get_compte_id_str_des(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicouser={}

		for tweet in cursor:
			ldes=[]
			user=tweet['user']['id_str']
			ldes.append(tweet['user']['id_str'])
			ldes.append(tweet['user']['name'])
			ldes.append(tweet['user']['screen_name'])
			ldes.append(tweet['user']['location'])
			ldes.append(tweet['user']['description'])
			ldes.append(tweet['user']['url'])
			ldes.append(tweet['user']['followers_count'])
			ldes.append(tweet['user']['friends_count'])
			ldes.append(tweet['user']['listed_count'])
			ldes.append(tweet['user']['created_at'])
			ldes.append(tweet['user']['verified'])
			ldes.append(tweet['retweet_count'])

			try:
				test= dicouser[user]
				
			except KeyError:
				dicouser[user]=ldes
		return dicouser

# les comptes qui ont emis un tweet ou un retwwet en langue avec leur nombre de followers
	def get_nbfollowers(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicofollowers={}
		for tweet in cursor:
			try : 
				user=tweet['user']['name']
				nbfollowers=tweet['user']['followers_count']
			except :
				print ('erreur', tweet['id'])
				pass


			try:
				dicofollowers[user]=nbfollowers  # if this account is already existing, we replace the number, no matter
				
			except KeyError:
				dicofollowers[user]=nbfollowers
		return dicofollowers


# compter les comptes et leur nombre de retweets uniquement  en langue 
	def get_compte_retweet(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang' : langue, 'retweeted_status': {"$ne" :None}}) 
		dicouser={}
		for tweet in cursor:
			user=tweet['user']['name']
			try:
				dicouser[user]=dicouser[user]+1
				
			except KeyError:
				dicouser[user]=1
		return dicouser

# compter les id comptes et leur nombre de retweets uniquement  en langue 
	def get_compte_retweet_id_str(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang' : langue, 'retweeted_status': {"$ne" :None}}) 
		dicouser={}
		for tweet in cursor:
			user=tweet['user']['id_str']
			try:
				dicouser[user]=dicouser[user]+1
				
			except KeyError:
				dicouser[user]=1
		return dicouser

# liste des organisations
	def get_orga(self):
		i=0
		musees = codecs.open(self._fileOrga, 'rb', "utf-8")

		list_m = []
		try :
			line = musees.readline()
			while line != '':
				lidorga = line.split(',')
				idorga= lidorga[0]
				list_m.append(idorga)
				line = musees.readline()
				i=i+1
		except :
			print ('erreur lecture Orga ligne', i)
		#print('-------------------------nombre d orga:', len(list_m))
		return list_m


# liste des organisations
	def get_orgabyName(self):
		i=0
		musees = codecs.open(self._fileOrga, 'rb', "utf-8")
		list_m = []
		try :
			line = musees.readline()
			while line != '':
				lidorga = line.split(',')
				idorga= lidorga[1]
				list_m.append(idorga)
				line = musees.readline()
				i=i+1
		except :
			print (' erreur lecture Orga ligne', i)
		return list_m

# compter le nombre de retweets uniquement  en langue des comptes de type Orga 
	def get_compte_retweetOrga(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})
		dicouser={}
		liste_orga=self.get_orga()
                #print liste_orga
		for tweet in cursor:
			try :
                            user=tweet['user']['id_str']
			except :
                            print ('erreur', tweet['id'])
                            pass			
			if user in liste_orga :
				try:
					dicouser[user]=dicouser[user]+1
					# print 'this user is an orga : ', user
				
				except KeyError:
					dicouser[user]=1
		return dicouser

# compter le nombre de tweets originaux uniquement  en langue des comptes de type Orga 
	def get_compte_tweetOrga(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})

		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': langue})
		dicouser={}
		liste_orga=self.get_orga()
		for tweet in cursor:
			try :
                            user=tweet['user']['id_str']
			except :
                            print ('erreur', tweet['id'])
                            pass                  
			if user in liste_orga :
				try:
					dicouser[user]=dicouser[user]+1
				
				except KeyError:
					dicouser[user]=1
		return dicouser

# compter le nombre de retweets uniquement  en langue des comptes de type Orga 
	def get_compte_retweetPrivate(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})
		dicouser={}
		liste_orga=self.get_orga()
		for tweet in cursor:
			user=tweet['user']['id_str']
			if user not in liste_orga :
				try:
					dicouser[user]=dicouser[user]+1
				
				except KeyError:
					dicouser[user]=1
		return dicouser

# lister les  tweets originaux uniquement  en langue des comptes de type Orga  et Private
	def list_tweetOrgaPrivate(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})
		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': langue})
		liste_orga=self.get_orga()
		dicoTweetOrga={}
		dicoTweetPrivate= {}
		for tweet in cursor :
			if tweet['user']['id_str'] in liste_orga :
				dicoTweetOrga[tweet['id_str']] = self.get_row_tweet(tweet)
			else :
				dicoTweetPrivate[tweet['id_str']] = self.get_row_tweet(tweet)	
		return [dicoTweetOrga, dicoTweetPrivate]



# compter le nombre de tweets originaux uniquement  en langue des comptes de type private 
	def get_compte_tweetPrivate(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})

		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': langue})
		dicouser={}
		liste_orga=self.get_orga()
		for tweet in cursor:
			user=tweet['user']['id_str']
			if user not in liste_orga :
				try:
					dicouser[user]=dicouser[user]+1
					# print 'this user is private :  ', user
				
				except KeyError:
					dicouser[user]=1
		return dicouser



# Tous les comptes qui ont emis un tweet ou un retwwet en langue
	def get_all_user(self, langue):
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})

		dicouser={}
		for tweet in cursor:
			user=tweet['user']['name']
			try:
				check=dicouser[user]   # existing account do nothing
				
			except KeyError:
				dicouser[user]=tweet['user']['id_str']  # creating account with name as key, is_str as value
# on ajoute les comptes mentionnes dans les retweets		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})
		for tweet in cursor:
			user=tweet['retweeted_status']['user']['name']
			try:
				check=dicouser[user]   # existing account do nothing				
			except KeyError:
				dicouser[user]=tweet['retweeted_status']['user']['id_str']

		return dicouser

# compter les comptes et leur nombre de tweets originaux en langue 
	def get_compte_tweet_originaux(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})

		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': langue})
		dicouser={}
		for tweet in cursor:
			user=tweet['user']['name']
			try:
				dicouser[user]=dicouser[user]+1
				
			except KeyError:
				dicouser[user]=1
		return dicouser

# compter les comptes et leur nombre de tweets originaux en langue 
	def get_compte_tweet_originaux_id_str(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})

		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': langue})
		dicouser={}
		for tweet in cursor:
			user=tweet['user']['id_str']
			try:
				dicouser[user]=dicouser[user]+1
				
			except KeyError:
				dicouser[user]=1
		return dicouser

# extraire les hashtags et les compter dans un dico
	def get_hashtags(self, langue):
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicohashtags={}
		for tweet in cursor:
			l=tweet['entities']['hashtags']
			dico={}
			
			for elem in l :
				dico=elem
				text=dico['text']
				try:
					dicohashtags[text]=dicohashtags[text]+1
				
				except KeyError:
					dicohashtags[text]=1
		return dicohashtags
		
# calculer les flux de hashtag par jour 
# creation d un dico ; clef : la date , valeur une liste de listes de type LA
# LA est une liste qui contient 2 elements [hastag, nombre]
	def get_fluxhashtags(self, langue):
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicoid={}
		
		for tweet in cursor:
			dicohashtag={}
			listehastagdunedateMAJ=[]
			datetweet=self.get_jma(self.get_date(tweet))	# la date du tweet
			listehashtagdutweet=tweet['entities']['hashtags']   # la liste des hashtags du tweet

			if listehashtagdutweet != [] :
				try:
					listehastagdunedate=dicoid[datetweet]
							# la date existe, il faut chercher si le hastag est dans la liste
				
					for ltag in listehashtagdutweet:
						hashtagexiste= False
						for l in  listehastagdunedate :
							dicohashtag = ltag
							texthashtag=dicohashtag['text']
							if l[0] == texthashtag :
						# le hashtag est dans la liste de la date
								l[1]=l[1]+1
								listehastagdunedateMAJ.append(l)
								hashtagexiste= True
										# on ext oblige de recopier la liste
							else :
								listehastagdunedateMAJ.append(l)
					
				
					#le hashtag n est pas dans liste on ajoute un element de 
						if hashtagexiste == False :
							l0=[texthashtag,1]
							listehastagdunedateMAJ.append(l0)
				# on met a jour la liste dans le dico
						
						dicoid[datetweet]=listehastagdunedateMAJ
						listehastagdunedateMAJ=[]

				except KeyError:
#           				# la date n existe pas on la cree avec une liste de hashtag du tweet
					listehastagdunedate=[]
					listehashtagdutweet=tweet['entities']['hashtags']
					for ltag in listehashtagdutweet :
						dicohashtag = ltag
						texthashtag=dicohashtag['text']
						l0=[texthashtag,1]
						listehastagdunedate.append(l0)
					dicoid[datetweet]=listehastagdunedate
                               	        
		# fin de boucle sur les tweets 
		return dicoid
		


# calculer le flux d'un hashtag par jour 
# creation d un dico ; clef : la date , valeur nombre du hashtag par jour
# if langue= all no filter else only the language langue 
	def get_fluxUnhashtag(self, langue, lehashtag):
		if langue == 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})

		dicoid={}
		dicohashtag={}
		for tweet in cursor:
			datetweet=self.get_jma(self.get_date(tweet))
			listehashtagdutweet=tweet['entities']['hashtags']
			for l in listehashtagdutweet :
				dicohashtag = l
				texthashtag=dicohashtag['text']
				if texthashtag == lehashtag :
					try:
						dicoid[datetweet]=dicoid[datetweet]+1		# date existe on increment le compte	
					except KeyError:
#           				# la date n existe pas on la cree 
						dicoid[datetweet]=1	
		# print 'creating dictionnary of tag : ' , lehashtag
		return dicoid
			

		
# verifie une stopliste de token	
	def get_checkToken(self, t) :
		if t.startswith('/') or t.startswith('@') or len(t) < 2 :
			return True

# extraire les tokens de tous les tweets (robuste) et les mettre dans un dico
	def get_tokens(self, langue) :	
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicotoken={}
		for tweet in cursor:
			texte =tweet['full_text']
			tokens = nltk.word_tokenize(texte.lower())
			flag = False
			for token in tokens:
				if token.startswith('/') or token.startswith('@') or len(token) < 2 :
					flag= True
				if flag is not True  :
					try:
						dicotoken[token]=dicotoken[token]+1
				
					except KeyError:
						dicotoken[token]=1
				flag=False
						
		return dicotoken

# extraire les tokens des tweets originaux des orgas (robuste) et les mettre dans un dico
	def get_tokensOriginalOrga (self, langue) :	
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})
		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': langue})
		dicotoken={}
		liste_orga=self.get_orga()
		for tweet in cursor:
			user=tweet['user']['id_str']
			if user  in liste_orga :   # on traite que les retweets des particuliers
				texte =tweet['full_text']
				tokens = nltk.word_tokenize(texte.lower())
				flag = False
				for token in tokens:
					if token.startswith('/') or token.startswith('@') or len(token) < 2 :
						flag= True
					if flag is not True  :
						try:
							dicotoken[token]=dicotoken[token]+1
				
						except KeyError:
							dicotoken[token]=1
					flag=False
						
		return dicotoken

# extraire les tokens des tweets originaux des privates (robuste) et les mettre dans un dico
	def get_tokensOriginalPrivate (self, langue) :	
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})
		else :
			cursor = self._collection.find({'retweeted_status': None,'lang': langue})
		dicotoken={}
		liste_orga=self.get_orga()
		for tweet in cursor:
			user=tweet['user']['id_str']
			if user not in liste_orga :   # on traite que les retweets des particuliers
				texte =tweet['full_text']
				tokens = nltk.word_tokenize(texte.lower())
				flag = False
				for token in tokens:
					if token.startswith('/') or token.startswith('@') or len(token) < 2 :
						flag= True
					if flag is not True  :
						try:
							dicotoken[token]=dicotoken[token]+1
				
						except KeyError:
							dicotoken[token]=1
					flag=False
						
		return dicotoken



# extraire les tokens des retweets (robuste) des particuliers et les mettre dans un dico
	def get_tokens_retweetParticuliers(self, langue) :	
	# extraire que les retweets dans la langue demande
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})

		dicotoken={}
		liste_orga=self.get_orga()			
		for tweet in cursor:
			user=tweet['user']['id_str']
			if user not in liste_orga :   # on traite que les retweets des particuliers
				texte =tweet['full_text']
				tokens = nltk.word_tokenize(texte.lower())
				flag = False
				for token in tokens:
					if token.startswith('/') or token.startswith('@') or len(token) < 2 :
						flag= True
					if flag is not True  :
						try:
							dicotoken[token]=dicotoken[token]+1
				
						except KeyError:
							dicotoken[token]=1
					flag=False
						
		return dicotoken

# extraire les tokens des retweets (robuste) des orga et les mettre dans un dico
	def get_tokens_retweetOrga(self, langue) :	
	# extraire que les retweets dans la langue demande
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})

		dicotoken={}
		liste_orga=self.get_orga()			
		for tweet in cursor:
			user=tweet['user']['id_str']
			if user  in liste_orga :   # on traite que les retweets des orga
				texte =tweet['full_text']
				tokens = nltk.word_tokenize(texte.lower())
				flag = False
				for token in tokens:
					if token.startswith('/') or token.startswith('@') or len(token) < 2 :
						flag= True
					if flag is not True  :
						try:
							dicotoken[token]=dicotoken[token]+1
				
						except KeyError:
							dicotoken[token]=1
					flag=False
						
		return dicotoken

# liste des tweets avec les comptes
	def allTweets(self, langue) :
			# on extrait les tweets qui sont  dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		dicoTweets={}
		# id_str est la cle du dico
		for tweet in cursor :
			dicoTweets[tweet['id_str']] = tweet['full_text']
		return dicoTweets


# liste des tweets  retweeted	
	def traitRetweet(self, langue) :
			# on extrait les tweets qui sont des retweets dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})
		#on affiche les id des user  et leur nom et leur nombre de tweets
		dicoid={}
		for tweet in cursor :
			dicoid[tweet['id_str']] = self.get_row_withIdtweet(tweet) 	
		return dicoid 

# qui est retweete par qui
	def quiRetweetQui(self, langue) :
		
		# on extrait les tweets qui sont des retweets dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})
		dicogephi={}

		for tweet in cursor:
		# creating dico : clef join two username separated by ___ and number of the suser retweet retweet 
			clef= tweet['retweeted_status']['user']['name']+'___'+ tweet['user']['name']
			try:
				dicogephi[clef]=dicogephi[clef]+1
				
			except KeyError:
				dicogephi[clef]=1  
		return dicogephi

# Scoring communauty of a list of accounts
#returning dico account, score
	def  score_subgraph(self,  langue ) :
		#building dico of all relayed
		dicorelayed=self.dicoRelayed(langue)
		#building dico of all relaying 
		dico=self.quiRetweetQui(langue)
		#for each account the list of account relayed
		dicosort = DictionnaireOrdonne()
		for k in dico.items():
			l=[]
			user=k[0].split('___')
			usera=user[0].encode('UTF-8')
			userb=user[1].encode('UTF-8')
			if dicosort.__contains__(usera):
				l=dicosort[usera]
				l.append(userb)
				dicosort[usera]=l
			else:
				l.append(userb)
				dicosort[usera]=l 
		dicosort.sort()
		# dicosort contains an account with the list of all accounts relayed by him
		# dicoScore contient compte, nombre de relayeurs de ce compte, nombre de relayeurs pas uniques de ce compte
		dicoScore={}
		# solution explorer les listes pas utilise mais  on garde au cas ou
		for k in dicosort.items():
			listedesRelayeur=k[1]
			trouve=False
			cptRelayeur=len(listedesRelayeur)
			l=[cptRelayeur]
			l.append(0)
			dicoScore[k[0]]=l # le nombre de compte qui ont relayed le compte k[0]
			# pour chaque compte verifier si ce compte a relayed d autres comptes
			for cp in listedesRelayeur:
				for k2 in dicosort.items() :
					if k[0] != k2[0] :
						listedesRelayeurtempo=k2[1]
						for ctempo in listedesRelayeurtempo :
							if cp == ctempo :
								trouve=True
								break
						if trouve :
							break
						else:
							continue				
			# on a explore tous le dico pour un compte
			# le compte n est pas unique relayeur
				if trouve  :
					l=dicoScore[k[0]]
					l[1]=l[1]+1
					dicoScore[k[0]]=l
				trouve=False
		# return dicoScore  # solution liste inhibee
		#  fin solution explore les listes
		# seconde solution compte  de relayed
		for k in dicosort.items():
			listedesRelayeur=k[1]
			l=[cptRelayeur]
			l.append(0)
			dicoScore[k[0]]=l # le nombre de compte qui ont relayed le compte k[0]
			# pour chaque compte verifier si ce compte a relayed d autres comptes
			for cp in listedesRelayeur:
				try:
					listerealyedcp=dicosort[cp]
					if len(listerealyedcp) >=1 :  # ce nest pas un compte unique
						l=dicoScore[k[0]]
						l[1]=l[1]+1
						dicoScore[k[0]]=l
				except KeyError:
					continue # compte cp inconnu on passe
			return dicoScore


# qui est retweete ou quoted par qui POUR NEO4J
	def quiRetweetQuiNeo4j(self, langue,outputfileneo4j, listeEdge) :
		# on extrait les tweets qui sont des retweets ou quoted dans la langue demandee
		# checking if classification exists
		countclass = self._collection.find({'lang': langue, 'classification': { "$ne" : None}}).count()
		if countclass == 0 :
			print ('WARNING end of checking classification NO CLASSIFICATION')
		else :
			print ('Everything fine, keep going')
		liste_orga=self.get_orga()
		with open(outputfileneo4j,'w') as f: 
			out = csv.writer(f, delimiter=',')
			# loop on type of link (retweeted or quoted)
			for modaledge in listeEdge :
				print ('processing : ', modaledge, ' for Neo4j input')
				if langue== 'all' :
					cursor = self._collection.find({modaledge : {"$ne" :None}})
				else :
					cursor = self._collection.find({'lang': langue, modaledge : {"$ne" :None}})

				print ('nombre de ', modaledge, ' pour neo4j dans la langue  choisie: ', cursor.count())
				tweetsansclassification=0		
				tweetsansclassification =0
				for tweet in cursor:
					# creating row : user_original, userdu retweet, id_tweet,date du retweet, hmseconde
					#findind date original tweet
					datetweetbrut=self.get_date_tweet_original2(tweet,modaledge)
					datetweet=self.get_jma(datetweetbrut)
					hmstweet=str(self.get_hms(datetweetbrut)[0])
					if tweet['user']['id_str'] in liste_orga:
						typeCompteduReTweet="Organisation"
					else :
						typeCompteduReTweet="Private"
					if tweet[modaledge ]['user']['id_str'] in liste_orga:
						typeCompteduTweet="Organisation"
					else :
						typeCompteduTweet="Private"

					dateretweet=self.get_jma(self.get_date(tweet))
					hmsretweet=str(self.get_hms(self.get_date(tweet))[0])
					# finding all classification of the original tweet (maxi 3)
					class1 = class2 = class3= "None"
					idtweetoriginal = tweet[modaledge]['id_str']
					if idtweetoriginal == None :
						print ('-------------------->  WARNING No original tweet')
					else :
						cursor1 = self._collection.find({'id_str' : idtweetoriginal})
						for t in cursor1 :
							l=[]
							try :
								classes= t['classification']
								l = self.getclassification(classes)

							except KeyError :
								l.append("NC")
								l.append("NC")
								l.append("NC")
								l.append(1)						
							row=[tweet[modaledge]['user']['name'], tweet[modaledge]['user']['id_str'],typeCompteduTweet, tweet['user']['name'],tweet['user']['id_str'],typeCompteduReTweet,tweet[modaledge]['id'],tweet[modaledge]['full_text'],datetweet, hmstweet,dateretweet,hmsretweet, l[0], l[1], l[2], modaledge]
							out.writerow(row)
							tweetsansclassification = tweetsansclassification + l[3]
						
			print (' nombre de tweets non classifiees : ', tweetsansclassification	)

# extraire les classifications de l attribut classification
	def getclassification(self, classes):
		lclasses=classes.split('_')
		class1 = class2 = class3= "NC"
		tweetsansclassification = 0
	# if tweet no classified we put None in fields classs
		if len(classes) == 0 :
			tweetsansclassification=1			
		else :
	# at least there is one classification 
			class1=lclasses[0]
			if len(lclasses) >1  :
				if lclasses[1] == "" :
					class2="NC"
				else :
					class2=lclasses[1]
				if len(lclasses) >2 :
					if lclasses[2] == "" :
						class3= "NC"
					else :
						class3= lclasses[2]
		l= [class1, class2, class3, tweetsansclassification]
		return l

# extraire les classifications de l attribut classification d' un tweet original
	def getclassificationfromtweetoriginal(self, idtweet):
		cursor1 = self._collection.find({'id_str' : idtweet})
		l=[]
		for t in cursor1 :
			try :
				classes= t['classification']
				l = self.getclassification(classes)

			except KeyError :
				l.append("NC")
				l.append("NC")
				l.append("NC")				
		return l


# computing edges
	def edges(self, langue,edgesFile, listeEdge) :
		# on extrait les tweets qui sont des reweet ou des quoted  dans la langue demandee
		liste_orga=self.get_orga()
		with open(edgesFile,'w') as edgef: 
			out = csv.writer(edgef, delimiter=',')
			row=["Source", "Target","Type", "Mode", "Class1", "Class2", "Class3"]
			out.writerow(row)
			for modaledge in listeEdge :
				if langue== 'all' :
					cursor = self._collection.find({modaledge : {"$ne" :None}})
				else :
					cursor = self._collection.find({'lang': langue, modaledge : {"$ne" :None}})

	#on ecrit les edges 
				print (' processing edges : ', modaledge)
			
# Source : le compte qui retweet ou quoted , Target le compte retwweted  ou quoted (auteur du tweet original)
				
				for tweet in cursor:
				# creating row : id_str from account who retweet
					typeEdge="Directed"
					typeEdge2=modaledge
				# extracting classification
					idtweetoriginal = tweet[modaledge ]['id_str']
					if idtweetoriginal == None :
						print ('-------------------->  WARNING No original tweet')

					l= self.getclassificationfromtweetoriginal(idtweetoriginal)
					if len(l) == 0 :
						l.append('NC')
						l.append('NC')
						l.append('NC')

					row=[tweet['user']['id_str'],tweet[modaledge ]['user']['id_str'],typeEdge, typeEdge2, l[0], l[1], l[2] ]
					out.writerow(row)

# -------------------------------------
# writing nodes
	def nodes(self, langue,nodesFile,  listeEdge) :
		print ("processing nodes  ")

			# building a dictionary of attributes of an account
		liste_orga=self.get_orga()
		dicoAlreadyProcessed={}
		with open(nodesFile,'w') as nodesf: 
			out = csv.writer(nodesf, delimiter=',')
			row=["Id", "Label","TypeAccount", "MentionedScore","RelayedScore","RelayingScore", "FollowersScore", "NbMedia"]
			out.writerow(row)
			for modaledge in listeEdge :
				print ("processing  ", modaledge)
				if langue== 'all' :
					cursor = self._collection.find({modaledge: {"$ne" :None}})
				else :
					cursor = self._collection.find({'lang': langue, modaledge: {"$ne" :None}})	
				
		
				type_compte="all"
			# on calcule les scores mentions de tous les comptes
				dicomention=self.mention(langue,type_compte)[2]
				dicorelayed=self.dicoRelayed(langue)
				dicorelaying=self.dicoRelaying(langue)
				dicofollowers=self.get_nbfollowers(langue)
				dicomedia=self.media(langue)
				for tweet in cursor:
				#processing the account which retweet 
					# creating row 
					#checking il alreday processed this account  
					if tweet['user']['id_str'] not in dicoAlreadyProcessed.keys() :

						if tweet['user']['id_str'] in liste_orga:
							typeCompte="Organisation"
						else :
							typeCompte="Private"
						try :
							mentionedScore=dicomention[tweet['user']['id_str']]
						except  KeyError :
							mentionedScore=0
						try :
							relayedScore=dicorelayed[tweet['user']['name']]
						except  KeyError :
							relayedScore=0
						try :
							relayingScore=dicorelaying[tweet['user']['name']]
						except  KeyError :
							relayingScore=0
						try :
							followersScore=dicofollowers[tweet['user']['name']]
						except  KeyError :
							followersScore=0
						try :
							mediaScore=dicomedia[tweet['user']['name']]
						except  KeyError :
							mediaScore=0

					# remembering writing
						dicoAlreadyProcessed[tweet['user']['id_str']]=True
						row=[tweet['user']['id_str'],tweet['user']['name'],typeCompte,mentionedScore,relayedScore,relayingScore,followersScore, mediaScore]
					# print "writing nodes original"
						out.writerow(row)

			#processing the retweeted or quoted account account 
				# creating row :
					if tweet[modaledge ]['user']['id_str'] not in dicoAlreadyProcessed.keys() :
						if tweet[modaledge ]['user']['id_str'] in liste_orga:
							typeCompte="Organisation"
						else :
							typeCompte="Private"
						try :
							mentionedScore=dicomention[tweet[modaledge ]['user']['id_str']]
						except  KeyError :
							mentionedScore=0
						try :
							relayedScore=dicorelayed[tweet[modaledge ]['user']['name']]
						except  KeyError  :
							relayedScore=0
						try :
							relayingScore=dicorelaying[tweet[modaledge ]['user']['name']]
						except  KeyError :
							relayingScore=0
						try :
							followersScore=dicofollowers[tweet[modaledge ]['user']['name']]
						except  KeyError :
							followersScore=0
						try :
							mediaScore=dicomedia[tweet[modaledge ]['user']['name']]
						except  KeyError :
							mediaScore=0

					# remembering writing
						dicoAlreadyProcessed[tweet[modaledge ]['user']['id_str']]=True
						row=[tweet[modaledge ]['user']['id_str'],tweet[modaledge ]['user']['name'],typeCompte,mentionedScore,relayedScore,relayingScore,followersScore, mediaScore ]
						out.writerow(row)
						# print "writing nodes reweet"
# ----------------------------

# building Nodes and Edges for Gephi

	def nodesAndEdgesforGephi (self, langue,nodesFile, edgesFile, listeEdge) :
# writing edges
		self.edges(langue, edgesFile,listeEdge)
# writing nodes
		self.nodes(langue, nodesFile, listeEdge)			
		return True

# building dictionary relayed account
	def dicoRelayed(self, langue):
		# on extrait les tweets qui sont des retweets dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})		
		dico={}			
		for tweet in cursor:
			clef= tweet['retweeted_status']['user']['name']
			try:
				dico[clef]=dico[clef]+1
				
			except KeyError:
				dico[clef]=1  
		return dico

# building dictionary relayed id_str account
	def dicoRelayed_id_str(self, langue):
		# on extrait les tweets qui sont des retweets dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})		
		dico={}			
		for tweet in cursor:
			clef= tweet['retweeted_status']['user']['id_str']
			try:
				dico[clef]=dico[clef]+1
				
			except KeyError:
				dico[clef]=1  
		return dico

# building dictionary relaying account
	def dicoRelaying(self, langue):
		# on extrait les tweets qui sont des retweets dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})		
		dico={}			
		for tweet in cursor:
			clef= tweet['user']['name']
			try:
				dico[clef]=dico[clef]+1
				
			except KeyError:
				dico[clef]=1  
		return dico

# building dictionary relaying account
	def dicoRelaying_id_str(self, langue):
		# on extrait les tweets qui sont des retweets dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})		
		dico={}			
		for tweet in cursor:
			clef= tweet['user']['id_str']
			try:
				dico[clef]=dico[clef]+1
				
			except KeyError:
				dico[clef]=1  
		return dico

# building dictionary quoted account
	def dicoQuoted(self, langue):
		# on extrait les tweets qui sont des tweets quoted dans la langue demandee: usage de commentaire dans le dispositif. on renvoie le nom du tweet qui a ete commente 
		if langue== 'all' :
			cursor = self._collection.find({'quoted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'quoted_status': {"$ne" :None}})		
		dico={}			
		for tweet in cursor:
			clef= tweet['quoted_status']['user']['name']
			try:
				dico[clef]=dico[clef]+1
				
			except KeyError:
				dico[clef]=1  
		return dico
				
# building list of relayeur
# dico clef = id_str_user name
# value : l[0] id_user_name, other elements are relayeur

	def create_list_relay(self, langue, outputfile) :
		# on extrait les tweets qui sont des retweets dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})

	# id des user  leur nom des originaux puis longueur de la chaine de relayeur
		dicorelay={}		
		for tweet in cursor:
			# creating row : user_original, userdu retweet
			l=[]
			clef=tweet['retweeted_status']['user']['id_str'] # le compte du tweet original
			trouve=False
			try:
				# le compte existe il faut ajouter le compte relayeur  dans la liste n'exsite pas
				l=dicorelay[clef]
				for e in l:
					if e==tweet['user']['name'] : # is the count in the list yet ? 
						trouve=True
						break
				if trouve==False :
					l.append(tweet['user']['name'])
					

			except KeyError:
					l.append(tweet['retweeted_status']['user']['name'])  #first element is the name of original user
					l0=[] #une nouvelle chaine
					l0.append(tweet['user']['name'])  # on ajoute le compte relayeur
					l.append(l0)
					dicorelay[clef]=l0   # dico[user]= [ [username], [un_relay]]	
		# writing result from dicorelay
		with open(outputfile,'wb') as f: 
			out = csv.writer(f,encoding='UTF-8', delimiter=',')
			for k in dicorelay.items() :
				# creating row : id-str_user, username, longueur de chaien de relais
				l=k[1]
				row=[k[0],l[0],len(l)]
				out.writerow(row)			

		return dicorelay



# analyse des pics
	def get_Pics(self, langue):		
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})

		print ("nb tweet fr traite pour les pics", cursor.count())
		for tweet in cursor:
			datetweet=self.get_date(tweet)

			
# Analyse des tweets
	def nbTweet(self, langue) :
		if langue== 'all' :
			cursor = self._collection.find( {'retweeted_status': None})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': None})
		return cursor.count()

# Analyse des retweets		
	def nbRetweet(self, langue) :
		if langue== 'all' :
			cursor = self._collection.find({ 'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})
		
		return cursor.count()


			

	def count_tweet(self, langue):	
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		return cursor.count()	
	    

#compter le nombre de compte qui ont rtweete un compte		

	def userRelayeur(self, dico) :
		dicouser_relayeur={}
		for k in dico.items():
			user=k[0].split('___')
			usera=user[0].encode('UTF-8')
			# print 'usera : ', usera
			try :
				dicouser_relayeur[usera]=dicouser_relayeur[usera]+1
			except KeyError :
				dicouser_relayeur[usera]=1
		return dicouser_relayeur

#les reply
	def reply(self, langue, outreply) :
		if langue== 'all' :
			cursor = self._collection.find({'in_reply_to_user_id': {"$ne" :None}})
		else :
			cursor = self._collection.find({'in_reply_to_user_id': {"$ne" :None}, 'lang':langue})
		with open(outreply,'w') as f: 
			out = csv.writer(f, delimiter=',')
			for tweet in cursor:
				# creating row : user_original, userdu reply, date
				datetweet=self.get_jma(self.get_date(tweet))
				row=[tweet['in_reply_to_screen_name'],tweet['in_reply_to_user_id_str'],tweet['user']['name'],tweet['user']['id_str'],datetweet]
				out.writerow(row)
		print ('reply done in file : ,', outreply)
		return True
# les photos
	def photo(self, langue) :
		if langue=='all' :
			cursor =self._collection.find()
		else :
			cursor = self._collection.find( {'lang':langue})
		dicophotos={}
   # le champ media est une liste dans Mongo, donc un traitement specifique pour recuperer le type
		for tweet in cursor :
			d=tweet['entities'].keys()
			if 'media' in d :
				l=tweet['entities']['media']
				dico=dict(l[0])	
				if dico['type']	== 'photo':
					urlphoto= dico['media_url']
					text=tweet['full_text']
					try :
						check=dicophotos[urlphoto]   # existing account add one in the list of text and count
						l=dicophotos[urlphoto]
						l[1]=l[1]+1
						dicophotos[urlphoto]=l
					except KeyError :
						l=[tweet['full_text'],1]
						dicophotos[urlphoto]=l
		

		return dicophotos

# producteurs de photos
	def producteursPhotos(self, langue) :
		if langue=='all' :
			cursor =self._collection.find()
		else :
			cursor = self._collection.find( {'lang':langue})
		dicoprod={}
   # le champ media est une liste dans Mongo, donc un traitement specifique pour recuperer le type
		for tweet in cursor :
			d=tweet['entities'].keys()
			if 'media' in d :
				l=tweet['entities']['media']
				dico=dict(l[0])	
				if dico['type']	== 'photo':
					try :
						user=tweet['user']['name']
						dicoprod[user]=dicoprod[user]+1
					except KeyError :
						dicoprod[user]=1
								
		return dicoprod


# les media score d un compte en nombre de photos
	def media(self, langue) :
		if langue=='all' :
			cursor =self._collection.find()
		else :
			cursor = self._collection.find( {'lang':langue})
		dicomedia={}
   # le champ media est une liste dans Mongo, donc un traitement specifique pour recuperer le type
		for tweet in cursor :
			d=tweet['entities'].keys()
			if 'media' in d :
				l=tweet['entities']['media']
				dico=dict(l[0])	
				if dico['type']	== 'photo':
					try :
						dicomedia[tweet['user']['name']]= dicomedia[tweet['user']['name']] +1
					except KeyError :
						dicomedia[tweet['user']['name']]=1		

		return dicomedia


# les comptes mentions
	def mention(self, langue, type_compte):	
		if langue=='all' :
			cursor =self._collection.find()
		else :
			cursor = self._collection.find( {'lang':langue})
		print ('traitement des mentions dans les comptes : ', type_compte)
		dicomention={}
		dicomentionid_str={}
		dicomentionneur={}

		liste_orga=self.get_orga()
		for tweet in cursor :
			l=tweet['entities']['user_mentions']
			i=0
		# qui sont les mentionneurs
			if len(l)> 0:
				mentionneur=tweet['user']['name']
				try:
					dicomentionneur[mentionneur]=dicomentionneur[mentionneur]+1
				except:
					dicomentionneur[mentionneur ]=1
		# boucle sur les mentions
			while i< len(l):  
				dico=dict(l[i])
				mention=dico['screen_name']
				mentionid_str=dico['id_str']
				user=tweet['user']['id_str']
			# que les comptes privates
				if type_compte == 'private' and  user  not in liste_orga :
					try :
						dicomention[mention]=dicomention[mention]+1
					except KeyError :
						dicomention[mention]=1
					try :
						dicomentionid_str[mentionid_str]=dicomentionid_str[mentionid_str]+1
					except KeyError :
						dicomentionid_str[mentionid_str]=1

			# que les comptes orga

				if type_compte == 'orga' and  user   in liste_orga :
					try :
						dicomention[mention]=dicomention[mention]+1
					except KeyError :
						dicomention[mention]=1
					try :
						dicomentionid_str[mentionid_str]=dicomentionid_str[mentionid_str]+1
					except KeyError :
						dicomentionid_str[mentionid_str]=1

			# tous  les comptes 
				if type_compte == 'all'  :
					try :
						dicomention[mention]=dicomention[mention]+1
					except KeyError :
						dicomention[mention]=1
					try :
						dicomentionid_str[mentionid_str]=dicomentionid_str[mentionid_str]+1
					except KeyError :
						dicomentionid_str[mentionid_str]=1

				i=i+1

		return [dicomention, dicomentionneur, dicomentionid_str]

# les comptes urls
	def url(self, langue, type_compte):	
		if langue=='all' :
			cursor =self._collection.find()
		else :
			cursor = self._collection.find( {'lang':langue})
		print ('traitement des url dans les comptes : ', type_compte)
		dicourl={}
		liste_orga=self.get_orga()
		for tweet in cursor :
			try:
				l=tweet['entities']['urls']
				i=0
				# boucle sur les url
				while i < len(l):
					dico=dict(l[i])
					url=dico['expanded_url']
					user=tweet['user']['id_str']
			# que les comptes privates
					if type_compte == 'private' and  user  not in liste_orga :
						try :
							dicourl[url]=dicourl[url]+1
						except KeyError :
							dicourl[url]=1
			# que les comptes orga
					if type_compte == 'orga' and  user   in liste_orga :
						try :
							dicourl[url]=dicourl[url]+1
						except KeyError :
							dicourl[url]=1
			# tous  les comptes 
					if type_compte == 'all'  :
						try :
							dicourl[url]=dicourl[url]+1
						except KeyError :
							dicourl[url]=1
					i=i+1
			
			# pas d url
			except KeyError :
				pass
		return dicourl


# searching tweets originaux
	def tweetOriginaux(self,  langue) :
		# on extrait les tweets qui sont des tweets originaux
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': None})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': None})
		dicogephi={}
	#on affiche le texte des  twwets originaux dans le dico et on compte 
		for tweet in cursor:
			# creating row :number of retweet, id_str of original tweet, text of oeriginal tweet, user name of original tweet account
			nbretweet=0
			#row=[tweet['full_text'],tweet['user']['name']]
			row=[tweet['full_text']]
			# creating dico : clef = id_str of original tweet
			clef= tweet['id_str']
			try:
				t=dicogephi[clef]
				dicogephi[clef]=row
				
			except KeyError:
				#first time (always)
				dicogephi[clef]=row
					  
		return dicogephi



# searching most rtweeted tweets
	def tweetMostRetweeted(self,  langue) :
		# on extrait les tweets qui sont des retweets
		# on extrait les tweets qui sont des retweets dans la langue demandee
		if langue== 'all' :
			cursor = self._collection.find({'retweeted_status': {"$ne" :None}})
		else :
			cursor = self._collection.find({'lang': langue, 'retweeted_status': {"$ne" :None}})
		dicogephi={}
	#on affiche les id des twwets originaux dans le dico et on compte 
		for tweet in cursor:
			# creating row :number of retweet, id_str of original tweet, text of oeriginal tweet, user name of original tweet account
			nbretweet=0
			row=[nbretweet,tweet['retweeted_status']['id_str'], tweet['retweeted_status']['full_text'],tweet['retweeted_status']['user']['name']]
			# creating dico : clef = id_str of original tweet
			clef= tweet['retweeted_status']['id_str']
			try:
				t=dicogephi[clef]
				# incrementing the number of retweet
				t[0]=t[0]+1
				dicogephi[clef]=t
				
			except KeyError:
				#first time
				row[0]=1
				dicogephi[clef]=row
					  
		return dicogephi

# extraire les textes s de tous les tweets  d orga correspondant aux criteres et les ecrire dans un fichier
	def writeTweets(self, langue,  file) :	
		f=open(file,'w')
		liste_orga=self.get_orga()
	# orga by orga
		for orga in liste_orga :
			s=f.write( '\n'+'****'+' *'+'nom_'+str(orga)+'\n')

			if langue== 'all' :
				cursor = self._collection.find()
			else :
				cursor = self._collection.find({'lang': langue})
			for tweet in cursor:
				user=tweet['user']['id_str']
				if user == orga :
					s=f.write(tweet['full_text'])
					s=f.write( '\n')
		f.close()
		return True					


# extraire les textes s de tous les tweets  d orga correspondant aux criteres et les ecrire dans un fichier
	def writeTweetsfromDico(self, langue,  file) :	
		f=open(file,'w')
		dicotweet={}
		emoji_pattern = re.compile("["
      					u"\U0001F600-\U0001F64F"  # emoticons
        				u"\U0001F300-\U0001F5FF"  # symbols & pictographs
      					u"\U0001F680-\U0001F6FF"  # transport & map symbols
					u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
					"]+", flags=re.UNICODE)

		urlpattern=re.compile("https:.*", flags=re.UNICODE)

		liste_orga=self.get_orga()
		if langue== 'all' :
			cursor = self._collection.find()
		else :
			cursor = self._collection.find({'lang': langue})
		for tweet in cursor:
			user=tweet['user']['id_str']
			if user in liste_orga :
				try :
					ltweet=dicotweet[user]
					ltweet.append(tweet['full_text'])
					dicotweet[user]=ltweet
				except :
					ltweet=[tweet['full_text']]
					dicotweet[user]=ltweet
		print ('nombre dico : ', len(dicotweet))
		for k in dicotweet.items() :	
			variable=str(k[0].replace(" ","_"))
			s=f.write( '\n'+'****'+' *'+'nom_'+variable+'\n')
			#s=f.write( '\n'+'****'+' *'+'nom_'+variable.encode('utf8')+'\n'.encode('utf8'))
			for t in k[1] :
				tclean=emoji_pattern.sub(r'',t)
				tclean2=urlpattern.sub(r'',tclean)
				s=f.write(tclean2)
				s=f.write( '\n')
		f.close()
		return True
# creation d'un fichier avec les scores des comptes
	def writeScore(self, langue, file) :
		f=open(file,'w')
		numero=1
		dicodouble={}
		liste_orga=self.get_orga()
		dicoCompte=self.get_compte_id_str(langue)
		dicoTweetOriginal=self.get_compte_tweet_originaux_id_str(langue)
		dicoRetweet=self.get_compte_retweet_id_str(langue)
		dicoRelayed=self.dicoRelayed_id_str(langue)
		dicoRelaying=self.dicoRelaying_id_str(langue)
		l=self.mention(langue,'all')
		dicoMention=l[0]
		#s=f.write('"Compte"'+','+'"type"'+','+'"NbOriginal"'+','+'"NbRetweet"'+','+'"Relayed"'+','+'"Relaying"'+','+'"Mentioned"'+','+'"Passing"'+'\n')
		for k in dicoCompte.items() :	
			if k[0] in liste_orga:
				type='organisation'
			else :
				type='private'
			try :
				a=dicoTweetOriginal[k[0]]
				original=a
			except KeyError :
				original=0
			try :
				a=dicoRetweet[k[0]]
				retweet=a
			except KeyError :
				retweet=0

			try :
				a=dicoRelayed[k[0]]
				relayed=a
			except KeyError :
				relayed=0
			try :
				a=dicoRelaying[k[0]]
				relaying=a
			except KeyError :
				relaying=0
			try :
				a=dicoMention[k[0]]
				mention=a
			except KeyError :
				mention=0


			passing=relayed*relaying
			numero=numero+1



			# s=f.write(k[0].encode('utf8')+','+type+','+str(original)+','+str(retweet)+','+str(relayed)+','+str(relaying)+','+str(mention)+','+str(passing)+'\n')
			if k[1] in dicodouble :
				pass
				print ('doublon : ' , k[0],k[1])
			else :
				dicodouble[k[1]]=1
				s=f.write(k[1]+','+type+','+str(original)+','+str(retweet)+','+str(relayed)+','+str(relaying)+','+str(mention)+','+str(passing)+'\n')

		f.close()

		


	
# FIN DE LA CLASSE	    


