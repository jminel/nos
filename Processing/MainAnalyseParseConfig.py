# -*- coding: utf-8 -*- 
#!/usr/bin/env python

# ################################################################################################
# 2nd hand owner: MoDyCo
# Adaptation: Brigitte Juanals, Jean-Luc Minel MoDyCo (Lyrics project + ComNum Labex)
# Licence: GNU GPL version 3 (temporary; askBrigitte Juanals or Jean-Luc Minel for final decision about Licence type)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Original owner: edsu
# Original name: 
# Original licence: CC0 1.0 Universal (Creative Commons Legal Code)
# Reference: 
# ################################################################################################
# Description | Table croise :
#  Appel des differentes classes qui traitent un flux de tweet
# # ------------------------------------------------------------------------------------------------
# Data type   | Mostly textual w/ multi-types.
# ------------------------------------------------------------------------------------------------
# Data format | Input: JSON (UTF-8 w/o unicode char); Output: CSV (UTF-8 w/o unicode char).
# ################################################################################################
import sys             # NEW (compared to the original github code)
import json
import codecs	       # NEW
import fileinput
import mongo
from pymongo import MongoClient
import re, nltk
import time
import os
if sys.version_info[0] < 3:
    import unicodecsv as csv
else:
    import csv

from  DictionnaireOrdonne import DictionnaireOrdonne
from AnalyseDate import AnalyseDate
from AnalyseFluxAny import AnalyseTweet
from Histolangue import AnalyseLangue


#parsing of filter and building dicodesfiltres
def parsefile(filename) :
	dicofiltre={}
	if os.path.exists(filename):
		with open(filename, 'rb') as f: 
			for line in f :
				line=line.decode("utf-8")		
				if str(line).startswith('#') :
					continue
				else :
					filter=str(line).split(':')
					print(filter)
					if filter[1].startswith("OUI") or filter[1].startswith("NON") :
						dicofiltre[filter[0]]=filter[1].split('\n')[0]
					else :
						print ("Modalite du filtre inconnue"+str(filter))
						exit()
		print ("valeurs des filtres lus dans le fichier :")
		for cle, valeur in dicofiltre.items():
			print (cle, valeur)
		return dicofiltre      
	
	elif IOError:
		print ("Unable to open file: "+str(filename))
		exit()
# -------------------------------------------------
# reading stopfile hashtag (excluding some hashtags)
def readingExcludedHashtags(filename) :
	dicoExcluded = {}
	if os.path.exists(filename):
		with open(filename, 'rb') as f: 
			for line in f :
				line=line.decode('utf-8').strip()		
				if str(line).startswith('#') :
					continue
				else :  #reading a hashtag
					#print("hashtag exclu ", line)
					dicoExcluded[line]= 1
	elif IOError:
		print ("Unable to open file oh hashatags excluded: "+str(filename))
		exit()

	print ("hashtag exclus --------------", len(dicoExcluded))
	for clef, value in dicoExcluded.items() :
		print(clef)
	return dicoExcluded 

# ----------------------------------------------

# parsing parameter file (db, collection, langue, datadir)
def parsefileconfig(filename):
	if os.path.exists(filename):
		listparameter = []
		with open(filename) as f :
			line = f.readline()
			filter = str(line).split(':') 
			inputMongoDb = filter[1]
			listparameter.append(inputMongoDb)
			line = f.readline()
			filter = str(line).split(':') 
			inputCollection = filter[1]
			listparameter.append(inputCollection )
			line = f.readline()
			filter = str(line).split(':') 
			langue = filter[1]
			listparameter.append(langue)
			line = f.readline()
			filter = str(line).split(':') 
			datadir = filter[1]
			listparameter.append(datadir)
			return listparameter
	elif IOError:
		print ("Unable to open file: "+str(filename))
		exit()


				
if __name__ == "__main__":
    start=time.time()
    print ("reading filters to run ")
    ftime= open("filetime.txt", "w")
    ftime.write(str(start))

    localtime = time.localtime(time.time())
    print ("Local current time :", localtime)

#     datadir est maintenant specifie dans le fichier configparse.txt

# les arguments input dans le fichier configparse.txt
    inputconfig = "./configParse.txt"
    liste = parsefileconfig(inputconfig)
    inputMongoDb=str(liste[0]).strip()  # NEW
    inputCollection=str(liste[1]).strip()  # NEW
    langue=str(liste[2]).strip() # NEW
    datadir = str(liste[3]).strip()
    print("Base : ",inputMongoDb, "Collection : ", inputCollection, "langue : ", langue, "directory results : ", datadir)

# lecture des filtres 
    fileFiltre="./filter.txt"
    print ("parsing file filter ")
    dicoFilter=parsefile(fileFiltre)

# analayse ----------------------------------------------------------------------------------	
    ana=AnalyseTweet(inputMongoDb,inputCollection)
    if dicoFilter["volumetrie"]=="OUI" :
        print ("processing ==> volumetrie")
        print ("_________________________________________________________________ ")
        print (" ATTENTION TRAITEMENT UNIQUEMENT  DES TWEETS RETWEETS  EN LANGUE  : ")
        print ('langue' , langue)
        print (" nombre de tweets  dans la base pour la langue demande :", langue,  ana.count_tweet(langue) )  
        print (" nombre de tweets originaux en langue : ", langue, '  ', ana.nbTweet(langue))
        print (" nombre de retweets en langue  : ", langue, '  ', ana.nbRetweet(langue) )
        print("nombre de comptes diffuseurs :" , len(ana.get_compte(langue)))
        print("_____________   FIN VOLUMETRIE ________________________")
    else :
        print ("pas de traitement de la volumetrie generale")

    faire=False
    if faire :
    	dico=ana.get_Pics(langue)
    	exit()

    if dicoFilter["tweets_originauxUniquement"]=="OUI" :
        print ("processing ==>  fichier tweets_originaux uniquement")
        dico=ana.tweetOriginaux(langue)
        outputfile = datadir+'tweetOriginauxUniquement.csv'
        print ("nombre de tweets originaux le fichier : ", outputfile , len(dico))
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t')
            for k in dico.items():
                row=[k[1][0]]
                out.writerow(row)
        f.close()
    else :
        print ("pas de traitement tweets_originaux uniquement")
#  ---------------------------------------------------------------------------------------
    if dicoFilter["tweets_originaux_retweeted"]=="OUI" :
        print ("processing ==>  tweets_originaux retweeted")
        dico=ana.tweetMostRetweeted(langue)
        outputfile = datadir+'tweetOriginauxRetweeted.csv'
        print ("nombre de tweets originaux retweetes dans le fichier :", outputfile , len(dico))
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t')
            for k in dico.items():
                row=[k[1][3],k[1][0],k[1][1],k[1][2]]
                out.writerow(row)
        f.close()
    else :
        print ("pas de traitement des tweets retweeted  ")

#  ---------------------------------------------------------------------------------------
    if dicoFilter["langues"]=="OUI" :
        print ("processing ==> langues")
        analang=AnalyseLangue(inputMongoDb,inputCollection)
        dicolang=analang.countlang()
        outputfile = datadir+'langues.csv'
        with open(outputfile,'w') as f: 
        	out = csv.writer(f, delimiter = '\t') 
        	for k in dicolang.items():
            	    row=[k[0],k[1]]	
            	    out.writerow(row)
        print ('liste des langues , resultat dans le fichier : ', outputfile)
        print ('nombre de langues : ', len(dicolang))

        print( " nombre de tweets originaux en langue : ", langue, '  ', ana.nbTweet(langue))
        print (" nombre de retweets en langue  : ", langue, '  ', ana.nbRetweet(langue))
    else :
        print ("pas de traitement des langues")   
#  ---------------------------------------------------------------------------------------
    if dicoFilter["hashtags"]=="OUI" :
        print ("processing ==>  hashtags")
        dico=ana.get_hashtags(langue)
        outputfile = datadir+'hashtags.csv'
        with open(outputfile ,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dico.items():
                row=[k[0],k[1]] 
                out.writerow(row)
        print ('liste des hashtags  en langue ', langue, ' resultat dans le fichier : ', outputfile )
    else :
        print ("pas de traitement des hashtags")

# --------------------------------------------------
    if dicoFilter["fluxhashtags"]=="OUI" :
        print ("processing ==>  fluxhashtags")
        ldico=ana.get_fluxhashtags(langue)
        dicodate = ldico[0]
        dicosemaine = ldico[1]
        outputfile1 = datadir+'fluxhashtagsJour.csv'
        outputfile2 = datadir+'fluxhashtagsSemaine.csv'
        with open(outputfile1 ,'w') as f: 
            out = csv.writer(f, delimiter = ',') 
            for clef, value in dicodate.items(): 
                for elem in value :
                    row = [clef, elem[0], elem[1]]
                    #print(row)
                    out.writerow(row)
        print ('flux des hastags par jour  en langue ', langue, ' resultat dans le fichier : ', outputfile1 )
        f.close()
        with open(outputfile2 ,'w') as f: 
            out = csv.writer(f, delimiter = ',') 
            row = ["Semaine","Hashtag", "Frequence"]
            out.writerow(row)
            for clef, value in dicosemaine.items():  
                for clefhasthag, number in value.items() :
                    row = [clef, clefhasthag, number ]
                    #print(row)
                    out.writerow(row)
        f.close()
        print ('flux des hastags par semaine  en langue ', langue, ' resultat dans le fichier : ', outputfile2 )
    else :
        print ("pas de traitement du flux des hashtags")


#  ---------------------------------------------------------------------------------------
    if dicoFilter["diffuseurs"]=="OUI" :
        print ("processing ==>  diffuseurs")
        dico=ana.get_compte_id_str(langue)
        nbAllCount = ana.get_number_tweets(langue)
        outputfile = datadir+'diffuseurs.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for clef, value in dico.items():
                    score = int(value[1])/nbAllCount*100			
                    row=[clef, value[0], value[1], score]    
                    out.writerow(row)
        print ('liste des comptes diffuseurs (producteurs et relayeurs), et nombre de tweets et retweets  emis resultat dans le fichier : ', outputfile)
        f.close()
    else :
        print ("pas de traitement des compes  diffuseurs")
#  ---------------------------------------------------------------------------------------

    if dicoFilter["producteurs"]=="OUI" :
        print ("processing ==>  producteurs")
        dico=ana.get_compte_producteurs(langue)
        nbTwoCount = ana.get_number_original_tweets(langue)
        outputfile = datadir+'producteurs.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for clef, value in dico.items():
                score = int(value[1])/nbTwoCount*100	
                row=[clef, value[0],value[1], score]     
                out.writerow(row)
        print ('liste des comptes producteurs, et nombre de tweets  emis resultat dans le fichier : ', outputfile)
    else :
        print ("pas de traitement des compes  diffuseurs")
#----------------------------------------------------------------------------

    if dicoFilter["descriptionComptes"]=="OUI" :
        print ("processing ==>  descriptions de tous les comptes diffuseurs")
        dico=ana.get_compte_id_str_des(langue)
        outputfile = datadir+'descriptionComptes.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '$') 
            for cle, value in dico.items():
                    row=[str(value[0]),value[1],value[4],value[7]]     
                    out.writerow(row)
        print (' descriptions de tous  les comptes diffuseurs : ', outputfile)
    else :
        print ("pas de traitement des descriptions des comptes")
#  ---------------------------------------------------------------------------------------
    if dicoFilter["producteursId_Name"]=="OUI" :
        print ("processing ==>  producteurs Id name")
        dico=ana. get_compte_id_str_name(langue)
        outputfile = datadir+'producteursIdName.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for id, name in dico.items():
                    row=[id, name]     
                    out.writerow(row)
        print ('liste des Id des comptes producteurs et leur nom dans : ', outputfile)
    else :
        print ("pas de traitement des Id des compes producteurs")

# ------------------------------------------------
    if dicoFilter["scoreEfficience"]=="OUI" :
        print ("processing ==>  score Efficience")
        dico=ana.forScoreEffi(langue)
        outputfile=datadir+'score_efficience.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f,delimiter = '\t')
            # row = ["idCompte","NameCompte", " nbtweetretweeted",    "sommeNBCompteRelayeur"] 
            for clef , value  in dico.items():
               	nbtweetretweeted = len(value)
                somme = 0
                sommeNBCompteRelayeur = 0
                #print("clef ", clef, "value ", value)
                for elem in value :
                    sommedesRetweets = somme + elem[2]
                    sommeNBCompteRelayeur  = sommeNBCompteRelayeur + elem[3]
                row = [clef, value[0][0], nbtweetretweeted, sommeNBCompteRelayeur]
                out.writerow(row)
        print ('calcul score efficience : ', outputfile)
    else :
        print( "pas de calcul score efficience ")
# ---------------------------------------------------------
# Un compte relayed ,  

    if dicoFilter["CompteRelayant"]=="OUI" :
        print ("processing ==>  le compte qui a retweeted et le nombre de fois")
        dico= ana.quiRetweetQui(langue)
        dicoNbCompteRelayant = {}
        outputfile=datadir+'CompteOriginalCompteQuiLeRetweetNombredeFois.csv'
        print ('champs :  Compteoriginal_name, ComptequiRetweet_name  nombre de retweet : ' , outputfile)
        with open(outputfile,'w') as f:
            out = csv.writer(f, delimiter = '\t') 
            for clef, value in dico.items():
			# on separe les comptes pour les mettre dans le fichier
                # compteRetweeteur=value[0][1]
                lclef = clef.split('___')
                idCompteOriginal = lclef[0]
                idCompteCompteQuiReweet = lclef[1]
                row=[idCompteOriginal ,value[0][0],idCompteCompteQuiReweet, value[0][1], value[1]]
                out.writerow(row)
                try :
                    l = dicoNbCompteRelayant[idCompteCompteQuiReweet]
                    l[1] = l[1]+ 1
                    dicoNbCompteRelayant[idCompteCompteQuiReweet] = l
                except KeyError : 
                    dicoNbCompteRelayant[idCompteCompteQuiReweet] = [value[0][1], 1]
        f.close()

        outputfile=datadir+'compteRelayant_NombreCompteRelayed.csv'
        with open(outputfile,'w', encoding='utf8') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for clef, value  in dicoNbCompteRelayant.items():
                row=[clef, value[0], value[1]]
                out.writerow(row)
        print ('Comptes relayant : Par nom du compte qui relay , le nombre de compte qu il  a relayed  dans le fichier :' , outputfile)



    else :
        print ("pas de traitement des relayed")
# -----------------------------------
#  ---------------------------------------------------------------------------------------
# Un compte relaying , le compte et le nombre de comptes qu'il a relayed   

    if dicoFilter["NombreTweetesRelayed"]=="OUI" :
        print ("processing ==>  les comptes  relaying")
        dicorelaying=ana.dicoRelaying(langue)
        outputfile=datadir+'compteRelayantNombreTweetRelayed.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for clef, value in dicorelaying.items():
                row=[clef, value[0], value[1]]
                out.writerow(row)
        print( 'Comptes relaying : Par account , le nombre de tweet qu il a relayed dans le fichier :' , outputfile)
    else :
        print ("pas de traitement du dicorelaying")


#  ---------------------------------------------------------------------------------------
    if dicoFilter["alltweets"]=="OUI" :
        print( "processing ==>  all tweets ")
        dico=ana.allTweets(langue)
        print ('liste des tweets  : ', 'AllTweets.csv')
        outputfile=datadir+'AllTweets.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dico.items():
                row=[k[0],k[1]]     
                out.writerow(row)
        f.close()

        l=ana.list_tweetOrgaPrivate(langue,)
        outputfile=datadir+'OrgaTweets.csv'
        print ('liste des tweets Orga : ', outputfile)
        dico= l[0]
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for key, value in dico.items():
                row=[key,value[0], value[1], value[2]]     
                out.writerow(row)
        f.close()

        outputfile=datadir+'PrivateTweets.csv'
        print ('liste des tweets Private : ', outputfile)
        dico= l[1]
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for key, value in dico.items():
                row=[key,value[0], value[1], value[2]]     
                out.writerow(row)
        f.close()
    else :
        print( "pas de liste des  tweets ")
#  ---------------------------------------------------------------------------------------


#  ---------------------------------------------------------------------------------------

#  ---------------------------------------------------------------------------------------
    if dicoFilter["producteurs_followers"]=="OUI" :
        print ("processing ==>  producteurs_followers")
        dico=ana.get_nbfollowers(langue)
        outputfile=datadir+'FollowersdesProducteurs.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t')
            for k in dico.items():
            	row=[k[0],k[1]]		
            	out.writerow(row)
        print ('liste des comptes ayant emis un tweet avec leurs followers :', langue,  ' resultat dans le fichier : ', outputfile)
    else :
        print ("pas de traitement des followers des producteurs")
#  ---------------------------------------------------------------------------------------
    if dicoFilter["producteurs_retweets_orga"]=="OUI" :
        print ("processing ==>  producteurs_retweets_orga")
        dico=ana.get_compte_retweetOrga(langue)
        outputfile=datadir+'nombreRetweets_Orga.csv'
        with open(outputfile,'w') as f: 
        	out = csv.writer(f, delimiter = '\t') 
        	for k in dico.items():
            		row=[k[0],k[1]]		
            		out.writerow(row)
        print ('liste des producteurs, et nombre de retweets des orga uniquement emis resultat dans le fichier : ', outputfile)
    else :
        print ("pas de traitement des retweets des comptes producteurs de type organisationnel")

#  ---------------------------------------------------------------------------------------
    if dicoFilter["producteurs_tweets_originaux_orga"]=="OUI" :
        print ("processing ==>  producteurs_tweets_originaux_orga")
        dico=ana.get_compte_tweetOrga(langue)
        outputfile=datadir+'nombreTweetsOriginauxOrga.csv'
        with open(outputfile,'w') as f: 
        	out = csv.writer(f, delimiter = '\t') 
        	for k in dico.items():
            		row=[k[0],k[1]]		
            		out.writerow(row)
        print ('nombre de producteurs  orga : ', len(dico))
        print ('liste des producteurs, et nombre de tweets originaux des orga  uniquement emis resultat dans le fichier : ', outputfile)
    else :
        print ("pas de traitement des comptes producteurs_tweets_originaux_orga")

#  ---------------------------------------------------------------------------------------
    if dicoFilter["producteurs_retweets_private"]=="OUI" :
        print ("processing ==>  producteurs_retweets_private")
        dico=ana.get_compte_retweetPrivate(langue)
        outputfile=datadir+'nombreReTweets_Private.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dico.items():
                row=[k[0],k[1]]		
                out.writerow(row)
        print ('nombre de producteurs retweets pivate : ', len(dico))
        print ('liste des producteurs, et nombre de retweets privates uniquement emis resultat dans le fichier : ', outputfile)
    else :
        print (" pas de traitement des comptes producteurs_retweets_private")
#  ---------------------------------------------------------------------------------------
    if dicoFilter["producteurs_tweets_originaux_private"]=="OUI" :
        print ("processing ==>  producteurs_tweets_originaux_private")
        dico=ana.get_compte_tweetPrivate(langue)
        outputfile=datadir+'nombreTweetsOriginaux_Private.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dico.items():
               row=[k[0],k[1]]		
               out.writerow(row)
        print ('nombre de producteurs  tweet private : ', len(dico))
        print ('liste des producteurs , et nombre de tweets originaux privates uniquement emis resultat dans le fichier : ', outputfile)
    else :
        print (" pas de traitement des comptes producteurs_tweets_originaux_private")
#  ---------------------------------------------------------------------------------------
    if dicoFilter["tokens"]=="OUI" :
        print ("processing ==>  tokens")
        dico=ana.get_tokens(langue)
        outputfile=datadir+'tokens.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f,delimiter = '\t')
            for k in dico.items():
            		row=[k[0],k[1]]		
            		out.writerow(row)
        print ( 'nombre de tokens : ', len(dico))
        print ('liste des tokens tous tweets en langue :', langue,  ' resultat dans le fichier : ', outputfile)
    else :
        print ("pas de traitement des tokens")
#  ---------------------------------------------------------------------------------------

    if dicoFilter["tokens_originaux_orga"]=="OUI" :
        print ("processing ==>  tokens_originaux_orga")
        dico=ana.get_tokensOriginalOrga(langue)
        outputfile=datadir+'tokenOriginalOrga.csv'
        with open(outputfile,'w') as f: 
        	out = csv.writer(f, delimiter = '\t')
        	for k in dico.items():
            		row=[k[0],k[1]]		
            		out.writerow(row)
        print ('nombre de tokens dans les tweets originaux organisation: ', len(dico))
        print ('liste des tokens tweets originaux organisations en langue :', langue,  ' resultat dans le fichier : ', outputfile)
    else :
        print (" pas de traitement des tokens_originaux_orga")
#  ---------------------------------------------------------------------------------------

    if dicoFilter["tokens_originaux_private"]=="OUI" :
        print ("processing ==>  tokens_originaux_private")
        dico=ana.get_tokensOriginalPrivate(langue)
        outputfile=datadir+'tokenOriginalPrivate.csv'
        with open(outputfile,'w') as f: 
        	out = csv.writer(f, delimiter = '\t')
        	for k in dico.items():
            		row=[k[0],k[1]]		
            		out.writerow(row)
        print ('nombre de tokens dans les tweets originaux  privates: ', len(dico))
        print( 'liste des tokens tweets originaux private en langue :', langue,  ' resultat dans le fichier : ', outputfile)
    else :
        print (" pas de traitement des tokens_originaux_private")
#  ---------------------------------------------------------------------------------------
  

    if dicoFilter["tokens_retweets_private"]=="OUI" :
        print ("processing ==>  tokens_retweets_private")
        dico=ana.get_tokens_retweetParticuliers(langue)
        outputfile=datadir+'token_retweetparticulier.csv'
        with open(outputfile,'w') as f: 
        	out = csv.writer(f, delimiter = '\t')
        	for k in dico.items():
            		row=[k[0],k[1]]		
            		out.writerow(row)
        print ('nombre de tokens retweet private : ', len(dico))
        print ('liste des tokens des retweets des particuliers en langue :', langue,  ' resultat dans le fichier : ', outputfile)
    else :
        print (" pas de traitement des tokens_retweets_private")

#  ---------------------------------------------------------------------------------------

    if dicoFilter["tokens_retweets_orga"]=="OUI" :
        print ("processing ==>  tokens_retweets_orga")
        dico=ana.get_tokens_retweetOrga(langue)
        outputfile=datadir+'token_retweetOrga.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t')
            print ('nombre de tokens : ', len(dico))
            for k in dico.items():
                    row=[k[0],k[1]]		
                    out.writerow(row)
        print ('nombre de tokens retweets orga: ', len(dico))
        print ('liste des tokens des retweets des orga  en langue :', langue,  ' resultat dans le fichier : ', outputfile)
    else :
        print (" pas de traitement des tokens_retweets_orga")
#  ---------------------------------------------------------------------------------------

  
    if dicoFilter["tweets_retweeted"]=="OUI" :
        print (" processing ==>  tweets_retweeted")
        dico=ana.traitRetweet(langue)
        outputfile=datadir+'TweetsRetweeted.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t')
            for k in dico.items():
                    row=[k[0],k[1][0],k[1][1],k[1][2],k[1][3],]		
                    out.writerow(row)	
        print ('nombre de retweets traite : ', len(dico))
        print ('liste des tweets  retweetes, resultat dans le fichier : ', outputfile)
    else :
        print (" pas de traitement des tweets_retweeted")
#  ---------------------------------------------------------------------------------------




# -------------------------------------------------------------------------------------------------------------------------
    if dicoFilter["qui_retweet_qui"]=="OUI" :
        print ("processing ==>  qui_retweet_qui")
        dico= ana.quiRetweetQui(langue)
        outputfile=datadir+'QuiretweeteQuiComptage.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for clef, value in dico.items():
                l = clef.split('__')
                usera_id = str(l[0])
                usera=value[0][0]
                userb=value[0][1]
                cpt=str(value[1])
                row=[usera_id, usera,userb,cpt]
                out.writerow(row)
        f.close()
        print ('Qui retweet qui consolide dans le fichier : ',  outputfile)
        outputfile2=datadir+'RelayedByAccountConsolidated.csv'
        with open(outputfile2,'w') as f :
            out = csv.writer(f, delimiter = '\t')
            dicoUser = {} # key is user_id_str
            for clef, value in dico.items() :
                l = clef.split('__')
                usera_id = str(l[0])
                usera = value[0][0]
                try :
                    l = dicoUser[usera_id]
                    l[1] = l[1]+ 1
                    dicoUser[usera_id] = l
                except KeyError :
                    dicoUser[usera_id] = [usera, 1]

            for clef, value in dicoUser.items() :
                row = [clef, value[0], value[1]]
                # print(row)
                out.writerow(row)
        f.close()
		
    else :
        print (" pas de traitement  de qui_retweet_qui")
# ---------------------------------------------------------

# Un compte passeur , le compte et le score passeur 

    if dicoFilter["passeur"]=="OUI" :
        print ("processing ==>  les comptes  passeur")
        dicoNbPasseur=ana.scorePasseur(langue)
        outputfile=datadir+'passeur.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for clef, value in dicoNbPasseur.items():
                row=[clef, value[0], value[1]]
                #print(row)
                out.writerow(row)
        print( 'Comptes passeurs : Par account , le score passeur :' , outputfile)
    else :
        print ("pas de traitement du score passeur")
#  ---------------------------------------------------------------------------------------
# Scoring communauty of an account
# checking if accounts N retweeting account A, retweet other accounts
# example : accounts which retweeted Juppe did not retweet others accounts, so there are not connected with the ecosystems
    if dicoFilter["scoring_subgraph"]=="OUI" :
        print ("processing ==>  ' Scoring_subgraph")
        dicoscore= ana.score_subgraph(langue)
        outputfile=datadir+'scorecommunauty.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicoscore.items():
                row=[k[0], k[1][0],k[1][1]]
                out.writerow(row) 
        print ("score de la communaute d'un compte fichier :  ", outputfile)

    else :
        print ("pas de traitement des scores des sous graphes")



# ------------------------------------------------------------------------------------

#  -----------------------------------------------------------------------------------------------------------------------------------------------
# Un compte quoted, le compte et le nombre de comptes qui l'ont quoted    

    if dicoFilter["quoted"]=="OUI" :
        print ("processing ==>  quoted")
        dicoquoted=ana.dicoQuoted(langue)
        outputfile=datadir+'compteQuoted.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicoquoted.items():
                    row=[k[0], k[1]]
                    out.writerow(row)
        print ('Comptes quoted : Par account , le nombre de compte qui l ont quoted dans le fichier :' , outputfile)
    else :
        print ("pas de traitement des quoted")

# on traite les dates	
#  -----------------------------------------------------------------------------------------------------------------------------------------------

    if dicoFilter["dates"]=="OUI" :
        print ("processing ==>  dates")
        print( "traitement des dates")
        fluxdate=AnalyseDate(inputMongoDb,inputCollection, langue)
        fluxdate.fluxjour(datadir, 'outpudate.csv')
        fluxdate.fluxjourtweet(datadir, 'outpudatetweet.csv')
        fluxdate.fluxjouretweet(datadir, 'outpudateretweet.csv')
        fluxdate.fluxSemaineTwo(datadir, 'outpudateTwoSemaine.csv')
        print ('Dates tweets :' , datadir+'outpudatetweet.csv')
        print ('Dates Retweets :' , datadir+'outpudateretweet.csv')

    else :
        print ("pas de traitement des dates")


#  -----------------------------------------------------------------------------------------------------------------------------------------------

#pour neo4j  

    if dicoFilter["grapheNeo4j"]=="OUI" :
        print ("processing ==>  grapheNeo4j")
        listeEdge=["retweeted_status", "quoted_status"]
        outputfile = datadir+'LinkNeo4j.csv'
        dico= ana.quiRetweetQuiNeo4j(langue,outputfile , listeEdge)
        print ('Qui retweet qui pour neo4j fichier : ',  datadir+'LinkNeo4j.csv')    
        dicouser= ana.get_all_user(langue)
        print ('nombre de producteurs avec compte : ', len(dicouser))
        outputfile=datadir+'allProducteursneo4j.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicouser.items():
                row=[k[0], k[1]]
                out.writerow(row)
        print ( 'liste de tous les  producteurs avec name et id_str, resultat dans le fichier : ', outputfile)
        print ('liste de tous les  liens entre compte (quoted ou retweeted: ', datadir+'LinkNeo4j.csv')
        print ('pensez a trier le fichier pour eliminer les doublons')
    else :
        print ("pas de traitement neo4j")

#  -----------------------------------------------------------------------------------------------------------------------------------------------

# les photos  

    if dicoFilter["photos"]=="OUI" :
        print ("processing ==>  photos")
        dicophoto=ana.photo(langue)
        outputfile=datadir+'photo.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicophoto.items():
                row=[k[0], k[1][0], k[1][1]]
                out.writerow(row)
        print ('url photo , le nombre d occurrence de cette url :' , outputfile)
    else :
        print ("pas de traitement des photos")

# producteur de photos 
#  -----------------------------------------------------------------------------------------------------------------------------------------------

    if dicoFilter["producteursPhotos"]=="OUI" :
        print ("processing ==>  producteurs de photos")
        dicophoto=ana.producteursPhotos(langue)
        outputfile=datadir+'producteursphotos.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicophoto.items():
                row=[k[0], k[1]]
                out.writerow(row)
        print ('producteurs  photo , le nombre de photos produites :' , outputfile)
    else :
       print ("pas de traitement des producteurs photos")

# les videos  
#  -----------------------------------------------------------------------------------------------------------------------------------------------

    if dicoFilter["videos"]=="OUI" :
        print ('processing ==>   videos')
        dicophoto=ana.photo(langue)
        outputfile=datadir+'video.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicophoto.items():
                row=[k[0], k[1][0], k[1][1]]
                out.writerow(row)
        print ('url video , le nombre d occurrence de cette url :' , outputfile)
    else :
        print ("pas de traitement videos")
#  -----------------------------------------------------------------------------------------------------------------------------------------------

# les mentions  
    if dicoFilter["mentions"]=="OUI" :
        print ("processing ==>  mentions")
        type_compte='all'
        liste=ana.mention(langue, type_compte)
        dicomention=liste[0]
        outputfile=datadir+'mentions.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicomention.items():
                row=[k[0], k[1]]
                out.writerow(row)
        print ('--- Mentions , le nombre d occurrences de cette mention :' , outputfile)
        dicomentionneur=liste[1]
        outputfile=datadir+'mentionneurs.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicomentionneur.items():
                row=[k[0], k[1]]
                out.writerow(row)
        print ('- Mentionneur , le nombre de fois ou ce compte mentionne des comptes dans ses tweets :' , outputfile)
    else :
        print ("pas de traitement des mentions")
#  -----------------------------------------------------------------------------------------------------------------------------------------------

# les url  

    if dicoFilter["url"]=="OUI" :
        print ("processing ==>  url")
        print ('traitement des url mentionnes dans les tweets')
        type_compte='all'
        dicourl=ana.url(langue, type_compte)
        outputfile=datadir+'urls.csv'
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for k in dicourl.items():
                row=[k[0], k[1]]
                out.writerow(row)
        print ('propriete url , le nombre d occurrences de cette url :' , outputfile)
    else :
        print ("pas de traitement des urls")



#  -----------------------------------------------------------------------------------------------------------------------------------------------
    if dicoFilter["tweets_orga"]=="OUI" :
        print ("processing ==>  tweets_orga")
        file=datadir+'twwetorga2.txt'
        print ('writing tweet from orga in ',file) 
        boo=ana.writeTweetsfromDico(langue, file)
    else :
        print ("pas de traitement des tweets orga")
#  -----------------------------------------------------------------------------------------------------------------------------------------------

    if dicoFilter["gephi"]=="OUI" :
        print ("processing ==>  gephi")
        nodefile  = datadir+'nodes.csv'
        edgefile = datadir+'edges.csv'
        listeEdge=["retweeted_status", "quoted_status"]
        print ('writing files for Gephi noeuds : ', nodefile , ' edges :  ', edgefile)
        ok=ana.nodesAndEdgesforGephi(langue, nodefile, edgefile, listeEdge)
    else :
        print ("pas de traitement pour Gephi")

#creation du fichier analyse rtsne
#  -----------------------------------------------------------------------------------------------------------------------------------------------
    if dicoFilter["rtsne"]=="OUI" :
        print ("processing ==>  rtsne")
        datartsnefile=datadir+'data.csv'
        print ('writing files for Tsne ',datartsnefile )
        ana.writeScore(langue,datartsnefile)
    else :
        print ("pas de traitement pour rtsne")


#  -----------------------------------------------------------------------------------------------------------------------------------------------
    if dicoFilter["iramuteq"]=="OUI" :
        print ("processing ==>  iramuteq")
        datafile=datadir+'corpustweet.txt'
        print ('writing files for Iramuteq ',datafile )
        ana.buildCorpusIramuteq(langue,datafile)
    else :
        print ("pas de traitement pour Iramuteq")
# ----------------------------------------------
    if dicoFilter["shared"]=="OUI" :
       	print ("processing ==>  shared accounts")
       	seuil = 30
       	l = ana.sharedAccountsDico(langue,seuil)
       	outputfile=datadir+'comptesShared.csv'
        dicoComptes = ana.get_compte_id_str_des(langue)
        with open(outputfile,'w') as f: 
            out = csv.writer(f, delimiter = '\t') 
            for elem in l:
                namea = dicoComptes[elem[0]]
                nameb = dicoComptes[elem[1]]
                row = [elem[0], namea[1],elem[1],nameb[1],elem[2]]
                out.writerow(row)
        print ('Comptes shared , lcomptes qui ont des comptes Linked en commun:' , outputfile)

    else :
        print ("pas de traitement pour shared")
# ---------------------------------------------------------

    if dicoFilter["couplehashtag"]=="OUI" :
       	print ("processing ==>  couple hashtag")
        fileExcluded = datadir + "hashtagsExclus.txt"
        dicoExcluded = readingExcludedHashtags(fileExcluded)
        l = ana.get_hashtagsLink(langue)
         # les nodes
        dicoNodes = l[0]
       	outputfile1=datadir+'nodesHashtags.csv'
        with open(outputfile1,'w') as f: 
            out = csv.writer(f, delimiter = ',') 
            row = ["Id","Label","Frequence"]
            out.writerow(row)
            #print(row)
            for clef, value in dicoNodes.items():
                idNode = value[0]
                row = [idNode,clef, value[1]]
                if ( (len(dicoExcluded) > 0) and (clef in dicoExcluded) ) :
                    #print("excluding", clef)
                    continue
                else :
                    #print(row)
                    out.writerow(row)
        f.close()
	
        # print ('Nodes hashtags :' , outputfile)
        # les arcs entre les nodes avec leur frequence
        listeEdges = l[1]
       	outputfile2=datadir+'EdgesHashtags.csv'
        with open(outputfile2,'w') as f: 
            out = csv.writer(f, delimiter = ',') 
            row = ["Source","Target", "Type", "labelH1", "labelH2", "CoOccurence"]
            out.writerow(row)
            nbcouplesExclus = 0 
            for elem in listeEdges :
                #print(elem)
                tmp1 = dicoNodes[elem[0]]
                labelH1 = elem[0]
                idNode1 = tmp1[0]
                tmp2 = dicoNodes[elem[1]]
                idNode2 = tmp2[0]
                labelH2 = elem[1]
                if ( (len(dicoExcluded) > 0) and ((labelH1 in dicoExcluded) or (labelH2  in dicoExcluded)) ) :
                    #print("excluding couple ", labelH1, labelH2  )
                    nbcouplesExclus = nbcouplesExclus +1 
                    continue
                else :
                    row = [idNode1,idNode2 ,"Undirected", labelH1, labelH2, elem[2]]
                    #print(row)
                    out.writerow(row)
        print("nombre de couples exclus ", nbcouplesExclus )
        print ('couple hashtag Done, files ' , outputfile1, outputfile2)
        f.close()

    else :
        print ("pas de traitement pour les couples de hashag")
# ------------------------------------------------------------------------------------------------

# on affiche le temps execution 
    localtime = time.localtime(time.time())
    print ("Local current time :", localtime)
    end = time.time() - start
    print ("Time to complete:", end)
    ftime.write(str(end))
    ftime.close()
    print ("Done")
